#!/bin/bash
# Usage: . ./patch_srpm.sh srpm_file spec_file original_text replacement_text
gsub_literal() {
  # STR cannot be empty
  [[ $1 ]] || return

  # string manip needed to escape '\'s, so awk doesn't expand '\n' and such
  awk -v str="${1//\\/\\\\}" -v rep="${2//\\/\\\\}" '
    # get the length of the search string
    BEGIN {
      len = length(str);
    }

    {
      # empty the output string
      out = "";

      # continue looping while the search string is in the line
      while (i = index($0, str)) {
        # append everything up to the search string, and the replacement string
        out = out substr($0, 1, i-1) rep;

        # remove everything up to and including the first instance of the
        # search string from the line
        $0 = substr($0, i + len);
      }

      # append whatever is left
      out = out $0;

      print out;
    }
  '
}

srpm=$(readlink -m ${1})
spec="${2}"
orig="${3}"
replace="${4}"
rpm -i "${srpm}"
cat "${HOME}/rpmbuild/SPECS/${spec}" | gsub_literal "${orig}" "${replace}" > "${HOME}/rpmbuild/SPECS/${spec}.new"
mv -f "${HOME}/rpmbuild/SPECS/${spec}.new" "${HOME}/rpmbuild/SPECS/${spec}"
pushd "${HOME}/rpmbuild"
  linux32 rpmbuild --quiet --target=i686 --define "_topdir $PWD" --define "dist .el7" --define "arch i686" --bs "SPECS/${spec}"
  mv -f SRPMS/*.src.rpm "${srpm}"
popd
rm -rf "${HOME}/rpmbuild"
