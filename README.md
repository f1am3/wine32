# 32 bit Wine packages for Enterprise Linux

**[Browse the yum repo.](https://harbottle.gitlab.io/wine32/7/i386)**

A yum repo of RPM files containing 32 bit Wine packages. The packages are
suitable for CentOS 7 (and RHEL, Oracle Linux, etc.).

Packages are rebuilt for 32 bit i686 from the standard Wine source RPMs
available in [EPEL](https://fedoraproject.org/wiki/EPEL) using
[GitLab CI](https://about.gitlab.com/gitlab-ci/). The yum repo is hosted
courtesy of [GitLab Pages](https://pages.gitlab.io/).

All packages [required to rebuild the wine32 source RPM](dependencies.md)
are also included in the repo.

All packages can also be installed alongside the 64 bit versions from EPEL.

## Quick Start

```bash
# Install repo
sudo yum -y install https://harbottle.gitlab.io/wine32/7/i386/wine32-release.rpm

# Install Wine 32 bit
sudo yum -y install wine.i686

# Run Wine 32 bit, for example:
wine32 cmd
```

## Installing 64 bit and 32 bit Wine together

```bash
# Install repos
sudo yum -y install epel-release https://harbottle.gitlab.io/wine32/7/i386/wine32-release.rpm

# Install Wine 64 bit and 32 bit
sudo yum -y install wine wine.i686

# Run Wine 64 bit, for example:
wine64 cmd

# Run Wine 32 bit, for example:
wine32 cmd
```
